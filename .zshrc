export EDITOR="nano"
export PATH=$PATH:/usr/bin/core_perl
export PATH=~/.local/bin:$PATH

# Como rodar o pacaur com e sem as confirmacoes?
alias pacaur-unsafe='pacaur'
alias pacaur='pacaur --noedit --noconfirm'

source $HOME/Programs/antigen/antigen.zsh
antigen use oh-my-zsh
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-completions
antigen theme robbyrussell
antigen apply

export DIRENV_LOG_FORMAT=
eval "$(direnv hook zsh)"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
